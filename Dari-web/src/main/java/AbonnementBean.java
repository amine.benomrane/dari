import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Entities.Abonnement;
import Services.AbonnementService;

@ManagedBean
@SessionScoped
public class AbonnementBean {

	private int idAbonnement;
	private String dateDeb;
	private String dateFin;
	private String state;
	private String image;
	private String type;
	private String idUser;

	private List<Abonnement> abonnements;

	@EJB
	private AbonnementService abonnementService;
	
	
	public String addAbonnement()  {
		abonnementService.Create(new Abonnement(1,dateDeb,dateFin,"En Cours",image,type,"a540fd0f-b13e-405e-9f45-4bb2216fe755") );
		return "Index.xhtml";
	}
	
	public String delAbonnement(Abonnement a) {
		abonnementService.Delete(a.getIdAbonnement());
		return "Index.xhtml";
	}

	public String modAbonnement(Abonnement a) {
		
		this.setIdAbonnement(a.getIdAbonnement());
		this.setDateDeb(a.getDateDeb());
		this.setDateFin(a.getDateFin());
		this.setImage(a.getImage());
		this.setState(a.getState());
		this.setType(a.getType());
		this.setIdUser(a.getIdUser());
		
		return "EditAbonnement.xhtml";
	}
	
	public String updateAbonnement() {
		
		Abonnement a = abonnementService.findById(idAbonnement);
		
		a.setDateDeb(dateDeb);
		a.setDateFin(dateFin);
		a.setImage(image);
		a.setState("En Cours");
		
		abonnementService.Update(a, idAbonnement);
		
		return "Index.xhtml";
	}
	
	public String confirmAbonnement(Abonnement abonnement) {
		
		abonnementService.Confirm(abonnement, abonnement.getIdAbonnement());
		return "Index.xhtml";
	}
	
	public String getAbonnementByID(Abonnement a) {

		this.setIdAbonnement(a.getIdAbonnement());
		this.setDateDeb(a.getDateDeb());
		this.setDateFin(a.getDateFin());
		this.setImage(a.getImage());
		this.setState(a.getState());
		this.setType(a.getType());
		this.setIdUser(a.getIdUser());
		return "MyAbonnement.xhtml";
	}
	
	@PostConstruct
	public void init() {
		setAbonnements(abonnementService.GetAll());
	}

	public List<Abonnement> getAbonnements() {
		abonnements = abonnementService.GetAll();
		return abonnements;
	}

	public void setAbonnements(List<Abonnement> abonnements) {
		this.abonnements = abonnements;
	}

	public int getIdAbonnement() {
		return idAbonnement;
	}

	public void setIdAbonnement(int idAbonnement) {
		this.idAbonnement = idAbonnement;
	}

	public String getDateDeb() {
		return dateDeb;
	}

	public void setDateDeb(String dateDeb) {
		this.dateDeb = dateDeb;
	}

	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	
	

}
