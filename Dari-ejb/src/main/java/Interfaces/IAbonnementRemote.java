package Interfaces;

import java.util.List;

import javax.ejb.Remote;

import Entities.Abonnement;

@Remote
public interface IAbonnementRemote {

	List<Abonnement> GetAll();
	public Abonnement findById(int id);
	public void Create(Abonnement a);
	public void Update(Abonnement a,int id);
	public void Delete(int id);
	public void Confirm(Abonnement a,int id);
}
