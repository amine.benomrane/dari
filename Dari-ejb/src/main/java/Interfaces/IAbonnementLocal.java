package Interfaces;

import java.util.List;

import javax.ejb.Local;

import Entities.Abonnement;

@Local
public interface IAbonnementLocal {

	List<Abonnement> GetAll();
	public Abonnement findById(int id);
	public void Create(Abonnement a);
	public void Update(Abonnement a,int id);
	public void Delete(int id);
}
