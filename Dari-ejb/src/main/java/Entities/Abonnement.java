package Entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Abonnement {

	@JsonProperty("IdAbonnementM")
	private int idAbonnement;
	
	@JsonProperty("DateDebutAbonnementM")
	private String dateDeb;
	
	@JsonProperty("DateFinAbonnementM")
	private String dateFin;
	
	@JsonProperty("StateM")
	private String state;
	
	@JsonProperty("ImageAbonnementM")
	private String image;
	
	@JsonProperty("typeM")
	private String type;
	
	@JsonProperty("UtilisateurId")
	private String idUser;
	
	public Abonnement() {
		super();
	}

	public Abonnement(int idAbonnement, String dateDeb, String dateFin, String state, String image, String type,
			String idUser) {
		super();
		this.idAbonnement = idAbonnement;
		this.dateDeb = dateDeb;
		this.dateFin = dateFin;
		this.state = state;
		this.image = image;
		this.type = type;
		this.idUser = idUser;
	}

	public int getIdAbonnement() {
		return idAbonnement;
	}

	public void setIdAbonnement(int idAbonnement) {
		this.idAbonnement = idAbonnement;
	}

	public String getDateDeb() {
		return dateDeb;
	}

	public void setDateDeb(String dateDeb) {
		this.dateDeb = dateDeb;
	}

	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	@Override
	public String toString() {
		return "Abonnement [idAbonnement=" + idAbonnement + ", dateDeb=" + dateDeb + ", dateFin=" + dateFin + ", state="
				+ state + ", image=" + image + ", type=" + type + ", idUser=" + idUser + "]";
	}
	
	
	
	
}
