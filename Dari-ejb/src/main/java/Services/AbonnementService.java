package Services;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.persistence.EntityManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;

import Entities.Abonnement;
import Interfaces.IAbonnementRemote;

/**
 * Session Bean implementation class AbonnementService
 */
@Stateless
@LocalBean
public class AbonnementService implements IAbonnementRemote {

	EntityManager em;

	public AbonnementService() {
		// TODO Auto-generated constructor stub
	}

	TrustManager[] noopTrustManager;

	@Override
	public List<Abonnement> GetAll() {

		List<Abonnement> listAbonnements = new ArrayList<Abonnement>();
		try {
			SSLContext sc = SSLContext.getInstance("ssl");
			sc.init(null, noopTrustManager, null);

			Client client = ClientBuilder.newBuilder().sslContext(sc).build();

			WebTarget web = client.target("https://localhost:44326/AbonnementApiController/GetAbonnements");
//			System.out.println(web);

			Response response = web.request().get();
			String result = response.readEntity(String.class);

//			System.out.println(result);

			JsonReader jsonReader = Json.createReader(new StringReader(result));
			JsonArray object = jsonReader.readArray();
//			System.out.println("-------Debut For---------");
			for (int i = 0; i < object.size(); i++) {

				Abonnement abonnement = new Abonnement();

				abonnement.setIdAbonnement(Integer.parseInt(object.getJsonObject(i).get("IdAbonnementM").toString()));
				abonnement.setImage(object.getJsonObject(i).get("ImageAbonnementM").toString());
				abonnement.setType(object.getJsonObject(i).get("typeM").toString());
				abonnement.setState(object.getJsonObject(i).get("StateM").toString());
				abonnement.setIdUser(object.getJsonObject(i).get("UtilisateurId").toString());

//				System.out.println(deb);

				String deb = object.getJsonObject(i).get("DateDebutAbonnementM").toString();
				deb = deb.substring(1, 11);
				abonnement.setDateDeb(deb);
//				System.out.println(deb);

				String fin = object.getJsonObject(i).get("DateFinAbonnementM").toString();
				fin = fin.substring(1, 11);
				abonnement.setDateFin(fin);

//	            Date finDate = null;
//	            try {
//	                String finStr = object.getJsonObject(i).get("DateFinAbonnementM").toString();
//	                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//	                finDate = sdf.parse(finStr);
//	                abonnement.setDateDeb(finDate);
//	            } catch (ParseException e) {
//
//	                e.printStackTrace();
//	            }

				listAbonnements.add(abonnement);
			}
//			System.out.print(listAbonnements.toString());
			return listAbonnements;

		} catch (Exception e) {
			// TODO: handle exception
			System.out.print(e);
			return listAbonnements;
		}
	}

	@Override
	public void Create(Abonnement a) {

		try {
			SSLContext sc = SSLContext.getInstance("ssl");
			sc.init(null, noopTrustManager, null);

			Client client = ClientBuilder.newBuilder().sslContext(sc).build();

			WebTarget web = client.target(
					"https://localhost:44326/api/AbonnementApi/AddAbPro?UtilisateurId=a540fd0f-b13e-405e-9f45-4bb2216fe755&"
							+ "StateM=En cours" + "&ImageAbonnementM=" + a.getImage() + "&DateDebutAbonnementM="
							+ a.getDateDeb() + "&DateFinAbonnementM=" + a.getDateFin() + "&IdAbonnementM="
							+ a.getIdAbonnement());
			System.out.println("---------Web Target--------");
			System.out.println(web);

			Response response = web.request().post(Entity.entity(a, MediaType.APPLICATION_JSON));
			System.out.println("---------Response--------");
			System.out.println(response);

			String result = response.readEntity(String.class);
			System.out.println("---------Result--------");
			System.out.println(result);

			response.close();
			System.out.println("Not catch ADD");
		} catch (Exception e) {
			System.out.println("Catch in service");
			System.out.println(e);
		}

	}

	@Override
	public Abonnement findById(int id) {

		ObjectMapper mapper = new ObjectMapper();
		try {
			SSLContext sc = SSLContext.getInstance("ssl");
			sc.init(null, noopTrustManager, null);

			Client client = ClientBuilder.newBuilder().sslContext(sc).build();

			WebTarget web = client.target("https://localhost:44326/api/AbonnementApi/GetAbonnementById/" + id);
//			System.out.println(web);

			Response response = web.request().get();
			System.out.println(response);

			String result = response.readEntity(String.class);
			System.out.println("----------result----------");
			System.out.println(result);

			Abonnement a = mapper.readValue(result, Abonnement.class);
			System.out.println("----------mapperObject----------");
			System.out.println(a.toString());

			return a;

		} catch (Exception e) {

			System.out.println("Exception by id : " + e);
			return null;
		}

	}

	@Override
	public void Update(Abonnement a, int id) {
		try {
			SSLContext sc = SSLContext.getInstance("ssl");
			sc.init(null, noopTrustManager, null);

			Client client = ClientBuilder.newBuilder().sslContext(sc).build();
			WebTarget target = client.target("https://localhost:44326/api/AbonnementApi/ModAbonnement?"
					+ "IdAbonnementM=" + id 
					+ "&ImageAbonnementM=" + a.getImage() 
					+ "&DateDebutAbonnementM=" + a.getDateDeb() 
					+ "&DateFinAbonnementM=" + a.getDateFin());

			Response response = target.request().put(Entity.entity(a, MediaType.APPLICATION_JSON));

			String result = response.readEntity(String.class);
			System.out.println(result);

			response.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void Delete(int id) {

		try {
			SSLContext sc = SSLContext.getInstance("ssl");
			sc.init(null, noopTrustManager, null);

			Client client = ClientBuilder.newBuilder().sslContext(sc).build();

			WebTarget target = client.target("https://localhost:44326/api/AbonnementApi/DelAbonnement/" + id);

			Response res = (Response) target.request().delete();
			System.out.println(res);

			res.close();
		} catch (Exception e) {
			System.out.println("Delete Error");
		}
	}

	@Override
	public void Confirm(Abonnement a,int id) {
		try {
			SSLContext sc = SSLContext.getInstance("ssl");
			sc.init(null, noopTrustManager, null);

			Client client = ClientBuilder.newBuilder().sslContext(sc).build();
			WebTarget target = client.target("https://localhost:44326/api/AbonnementApi/ConfirmAbonnement/"+id);
			Response response = target.request().put(Entity.entity(a, MediaType.APPLICATION_JSON));

			String result = response.readEntity(String.class);
			System.out.println(result);

			response.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

}
